# Sucursal CRUD

* CRUD desarrollado en Spring Boot y MySQL.

## Herramientas
* IntelliJ IDEA Ultimate
* JDK 1.8
* Gradle 4+
* Xampp 7.4.5


## Paso a paso
* Habilitar apache server y mysql en panel de control de xampp.
* Para ejecutar la aplicación, ejecute el siguiente comando en una ventana de terminal (directorio raíz):
```java
gradlew bootRun
```

## Dependencias

* Spring Web
    * Build web, including RESTful, applications using Spring MVC. Uses Apache Tomcat as the default embedded container.
* Spring Security
    * Highly customizable authentication and access-control framework for Spring applications.
* Spring Data JPA
    * Persist data in SQL stores with Java Persistence API using Spring Data and Hibernate.
* H2 Database
    * Provides a fast in-memory database that supports JDBC API and R2DBC access, with a small (2mb) footprint. Supports embedded and server modes as well as a browser based console application.
* MySQL Driver
    * MySQL JDBC and R2DBC driver.
* Swagger
    * API documentation for spring.



## Comandos para gradle/Spring

```java
gradlew clean: 		limpia el directorio de compilación generado
gradlew bootRun: 	descarga las dependencias y ejecuta la aplicación
gradlew test:		ejecuta las pruebas
```

## API - END POINTS
* Documentación Swagger: http://localhost:8080/swagger-ui/index.html

* Postman collection: https://www.getpostman.com/collections/b2052c950670086f4374

### **(GET) /api/sucursales**
* Descripción: obtener todas las sucursales.
* Ejemplo: */api/sucursales*
* Parámetros: -
* Cuerpo de la petición: -
* Response:
    * Code: OK
    * Status: 200
```javascript
[
    {
        "id": 1,
        "direccion": "Donado 65, Bahia Blanca",
        "latitud": -38.62535259870338,
        "longitud": -62.27238908987615
    },
    {
        "id": 2,
        "direccion": "Tucuman 1162, General Roca",
        "latitud": -38.701981821877965,
        "longitud": -67.56873950583062
    },
    {
        "id": 3,
        "direccion": "San Martin 341, Chubut",
        "latitud": -45.52113552989464,
        "longitud": -67.45887623152842
    },
    {
        "id": 4,
        "direccion": "Coronel Gil 267, La Pampa",
        "latitud": -36.33212878305667, 
        "longitud": -64.20692331218325
    }
]
```

### **(POST) /api/sucursales**
* Descripción: crear sucursal.
* Ejemplo: */api/sucursales*
* Parámetros: -
* Cuerpo de la petición:
```javascript
    {
        "direccion": "Donado 65",
        "latitud": -38.62535259870338,
        "longitud": -62.27238908987615
    }
```
* Response:
    * Code: Created
    * Status: 201
```javascript
    {
        "id": 1,
        "direccion": "Donado 65",
        "latitud": -38.62535259870338,
        "longitud": -62.27238908987615
    }
```

### **(PUT) /api/sucursales**
* Descripción: modificar sucursal.
* Ejemplo: */api/sucursales*
* Parámetros: -
* Cuerpo de la petición:
```javascript
    {
        "id": 1,
        "direccion": "Donado 1065",
        "latitud": -38.62535259870338,
        "longitud": -62.27238908987615
    }
```
* Response:
    * Code: OK
    * Status: 200
```javascript
    {
        "id": 1,
        "direccion": "Donado 1065",
        "latitud": -38.62535259870338,
        "longitud": -62.27238908987615
    }
```

### **(GET) /api/sucursales/{id}**
* Descripción: obtener la sucursal con id {id}.
* Ejemplo: */api/sucursales/1*
* Parámetros: id de sucursal.
* Cuerpo de la petición: -
* Response:
    * Code: OK
    * Status: 200
```javascript
    {
        "id": 1,
        "direccion": "Donado 1065",
        "latitud": -38.62535259870338,
        "longitud": -62.27238908987615
    }
```

### **(DELETE) /api/sucursales/{id}**
* Descripción: eliminar la sucursal con id {id}.
* Ejemplo: */api/sucursales/1*
* Parámetros: id de sucursal.
* Cuerpo de la petición: -
* Response:
    * Code: OK
    * Status: 200

### **(GET) /api/sucursales/{latitude}/{longitude}**
* Descripción: obtener la sucursal más cercana a un punto dado.
* Ejemplo: */api/sucursales/-40.80820814870802/-62.99644703062799*
* Parámetros: latitud y longitud del punto.
* Cuerpo de la petición: -
* Responses:
    * Code: OK
    * Status: 200
```javascript
    {
        "id": 1,
        "direccion": "Donado 1065",
        "latitud": -38.62535259870338,
        "longitud": -62.27238908987615
    }
```
