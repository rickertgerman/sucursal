package rickert.german.sucursal.repository;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import rickert.german.sucursal.model.Sucursal;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

/**
 * @author: Rickert German
 * @version: 16/05/2021
 */
@DataJpaTest
class SucursalRepositoryTest {

    @Autowired
    private SucursalRepository underTest;

    @AfterEach
    void tearDown() {
        underTest.deleteAll();
    }

    @Test
    void itShouldCheckWhenSucursalIdExits() {
        // given
        String direccion = "San Martín 458";
        Sucursal sucursal = new Sucursal(direccion, -38.62535259870338, -62.27238908987615);
        Sucursal newSucursal = underTest.save(sucursal);

        // when
        boolean expected = underTest.selectExistsById(newSucursal.getId());

        // then
        assertThat(expected).isTrue();
    }

    @Test
    void itShouldCheckWhenSucursalIdDoesNotExits() {
        // given
        Long id = 5L;

        // when
        boolean expected = underTest.selectExistsById(id);

        // then
        assertThat(expected).isFalse();
    }

    @Test
    void itShouldCheckWhenSucursalDireccionExits() {
        // given
        String direccion = "San Martín 458";
        Sucursal sucursal = new Sucursal(direccion, -38.62535259870338, -62.27238908987615);
        Sucursal newSucursal = underTest.save(sucursal);

        // when
        boolean expected = underTest.selectExistsByDireccion(newSucursal.getDireccion());

        // then
        assertThat(expected).isTrue();
    }

    @Test
    void itShouldCheckWhenSucursalDireccionDoesNotExits() {
        // given
        String direccion = "Las Heras 45";

        // when
        boolean expected = underTest.selectExistsByDireccion(direccion);

        // then
        assertThat(expected).isFalse();
    }
}
