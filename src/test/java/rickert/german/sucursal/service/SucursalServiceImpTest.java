package rickert.german.sucursal.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import rickert.german.sucursal.exceptions.InvalidFieldException;
import rickert.german.sucursal.exceptions.NotFoundException;
import rickert.german.sucursal.model.Sucursal;
import rickert.german.sucursal.repository.SucursalRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

/**
 * @author: Rickert German
 * @version: 16/05/2021
 */
@ExtendWith(MockitoExtension.class)
class SucursalServiceImpTest {

    @Mock
    private SucursalRepository sucursalRepository;
    private SucursalService underTest;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        underTest = new SucursalServiceImp(sucursalRepository);
    }

    @Test
    void findAll() {
        // when
        underTest.findAll();

        // then
        verify(sucursalRepository).findAll();
    }

    @Test
    void findById() throws NotFoundException {
        // given
        Long id = 1L;
        Sucursal sucursal = new Sucursal(id, "Donado 65", -38.62535259870338, -62.27238908987615);

        given(sucursalRepository.findById(id)).willReturn(Optional.of(sucursal));

        // when
        Sucursal sucursal1 = underTest.findById(id);

        // then
        assertThat(sucursal1).isEqualTo(sucursal);
    }

    @Test
    void willThrowWhenFindById() {
        // given
        Long id = 1L;

        given(sucursalRepository.findById(id)).willReturn(Optional.empty());

        // when
        // then
        assertThatThrownBy(() -> underTest.findById(id))
                .isInstanceOf(NotFoundException.class)
                .hasMessageContaining("No se encontró sucursal con Id " + id + ".");
    }

    /**
     * Caso exitoso de para la operación de creación de sucursal
     */
    @Test
    void canCreateSucursal() throws InvalidFieldException {
        // given
        String direccion = "San Martín 154";
        Sucursal sucursal = new Sucursal(direccion, -38.62535259870338, -62.27238908987615);

        // when
        underTest.create(sucursal);

        // then
        ArgumentCaptor<Sucursal> sucursalArgumentCaptor = ArgumentCaptor.forClass(Sucursal.class);

        verify(sucursalRepository).save(sucursalArgumentCaptor.capture());

        Sucursal capturedSucursal = sucursalArgumentCaptor.getValue();

        assertThat(capturedSucursal).isEqualTo(sucursal);
    }

    /**
     * Caso NO exitoso de para la operación de creación de sucursal
     */
    @Test
    void whillThrowWhenCreateSucursal() {
        // given
        String direccion = "San Martín 154";
        Sucursal sucursal = new Sucursal(direccion, -38.62535259870338, -62.27238908987615);

        given(sucursalRepository.selectExistsByDireccion(direccion)).willReturn(true);

        // when
        // then
        assertThatThrownBy(() -> underTest.create(sucursal))
                .isInstanceOf(InvalidFieldException.class)
                .hasMessageContaining("Ya existe Sucursal con la dirección proporcionada.");

        verify(sucursalRepository, never()).save(any());
    }

    @Test
    void canUpdateSucursal() throws NotFoundException {
        // given
        Long id = 1L;
        Sucursal sucursal = new Sucursal(id, "San Martín 154", -38.62535259870338, -62.27238908987615);

        given(sucursalRepository.selectExistsById(id)).willReturn(true);

        // when
        sucursal.setDireccion("Las Heras 45");
        underTest.update(sucursal);

        // then
        ArgumentCaptor<Sucursal> sucursalArgumentCaptor = ArgumentCaptor.forClass(Sucursal.class);

        verify(sucursalRepository).save(sucursalArgumentCaptor.capture());

        Sucursal capturedSucursal = sucursalArgumentCaptor.getValue();

        assertThat(capturedSucursal).isEqualTo(sucursal);
    }

    @Test
    void willThrowWhenIdDoesNotExists() {
        // given
        Long id = 1L;
        Sucursal sucursal = new Sucursal(id, "San Martín 154", -38.62535259870338, -62.27238908987615);

        given(sucursalRepository.selectExistsById(anyLong())).willReturn(false);

        // when
        // then
        sucursal.setDireccion("Las Heras 45");
        assertThatThrownBy(() -> underTest.update(sucursal))
                .isInstanceOf(NotFoundException.class)
                .hasMessageContaining("No se encontró sucursal con Id " + sucursal.getId() + ".");

        verify(sucursalRepository, never()).save(any());
    }

    @Test
    void deleteById() throws NotFoundException {
        // given
        Long id = 1L;

        given(sucursalRepository.selectExistsById(id)).willReturn(true);

        // when
        underTest.deleteById(id);

        // then
        verify(sucursalRepository, times(1)).deleteById(any());
    }

    @Test
    void willThrowWhenDeleteById() {
        // given
        Long id = 1L;

        given(sucursalRepository.selectExistsById(id)).willReturn(false);

        // when
        // then
        assertThatThrownBy(() -> underTest.deleteById(id))
                .isInstanceOf(NotFoundException.class)
                .hasMessageContaining("No se encontró sucursal con Id " + id + ".");

        verify(sucursalRepository, never()).deleteById(any());
    }

    /**
     * Caso exitoso de para la operación de cálculo de distancias
     */
    @Test
    void findNearestSucursal() throws NotFoundException {
        // given
        double lat = -40.80820814870802;
        double lon = -62.99644703062799;

        List<Sucursal> sucursalList = new ArrayList<>();
        Sucursal sucursalEsperada = new Sucursal(3L, "Sarmiento 299", -40.80523982021187, -62.991707183891855);
        sucursalList.add(new Sucursal(1L, "Donado 65", -38.62535259870338, -62.27238908987615));
        sucursalList.add(new Sucursal(2L, "Tucuman 1162", -38.701981821877965, -67.56873950583062));
        sucursalList.add(sucursalEsperada);
        sucursalList.add(new Sucursal(4L, "San Martín 341", -45.62879771667928, -67.41493092180755));
        sucursalList.add(new Sucursal(5L, "Onelli 407", -40.87963839355875, -71.4139541064077));
        given(sucursalRepository.findAll()).willReturn(sucursalList);

        // when
        Sucursal nearestSucursal = underTest.findNearestSucursal(lat, lon);

        // then
        assertThat(nearestSucursal).isEqualTo(sucursalEsperada);
    }

    /**
     * Caso NO exitoso de para la operación de cálculo de distancias
     */
    @Test
    void willThrowWhenSucursalesAreEmpty() {
        // given
        double lat = -40.80820814870802;
        double lon = -62.99644703062799;

        List<Sucursal> sucursalList = new ArrayList<>();
        given(sucursalRepository.findAll()).willReturn(sucursalList);

        // when
        // then
        assertThatThrownBy(() -> underTest.findNearestSucursal(lat, lon))
                .isInstanceOf(NotFoundException.class)
                .hasMessageContaining("No se encontraron Sucursales.");
    }
}