package rickert.german.sucursal.exceptions;

public class InvalidFieldException extends Exception {
    public InvalidFieldException(String msg) {
        super(msg);
    }
}