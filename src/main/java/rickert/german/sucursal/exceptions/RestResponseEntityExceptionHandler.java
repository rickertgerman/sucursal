package rickert.german.sucursal.exceptions;

import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import rickert.german.sucursal.model.ErrorResponse;
import rickert.german.sucursal.utils.Constants;

import javax.servlet.http.HttpServletResponse;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

/**
 * Clase que proporciona un manejo centralizado de excepciones.
 *
 * @author: Rickert German
 * @version: 15/05/2021
 */
@ControllerAdvice
@RestController
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
    private static final Logger LOGGER = Logger.getLogger(RestResponseEntityExceptionHandler.class.getName());

    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        this.showExceptionMessage(ex);
        return getResponseEntity(request, HttpServletResponse.SC_NOT_FOUND, Constants.HTTP_SERVLET_RESPONSE_NOT_FOUND, Constants.ERROR_MESSAGE_NOT_FOUND_URL, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(DataAccessException.class)
    public final ResponseEntity<Object> handleDataAccessException(DataAccessException ex, WebRequest request) {
        this.showExceptionMessage(ex);
        return getResponseEntity(request, HttpServletResponse.SC_BAD_REQUEST, Constants.HTTP_SERVLET_RESPONSE_BAD_REQUEST, Constants.ERROR_MESSAGE_MISSING_PARAMETERS, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NotFoundException.class)
    public final ResponseEntity<Object> handleNotFoundException(NotFoundException ex, WebRequest request) {
        this.showExceptionMessage(ex);
        return getResponseEntity(request, HttpServletResponse.SC_NOT_FOUND, Constants.HTTP_SERVLET_RESPONSE_NOT_FOUND, ex.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(InvalidFieldException.class)
    public final ResponseEntity<Object> handleInvalidFieldException(InvalidFieldException ex, WebRequest request) {
        this.showExceptionMessage(ex);
        return getResponseEntity(request, HttpServletResponse.SC_BAD_REQUEST, Constants.HTTP_SERVLET_RESPONSE_BAD_REQUEST, ex.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(HttpServerErrorException.InternalServerError.class)
    public final ResponseEntity<Object> handleInternalServerError(HttpServerErrorException.InternalServerError ex, WebRequest request) {
        this.showExceptionMessage(ex);
        return getResponseEntity(request, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, Constants.HTTP_SERVLET_RESPONSE_INTERNAL_SERVER_ERROR, Constants.ERROR_MESSAGE_INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * All Exceptions
     */
    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
        LOGGER.log(Level.SEVERE, ex.getMessage());
        return getResponseEntity(request, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, Constants.HTTP_SERVLET_RESPONSE_INTERNAL_SERVER_ERROR, Constants.ERROR_MESSAGE_INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private ResponseEntity<Object> getResponseEntity(WebRequest request, int httpServletResponseNumber, String httpServletResponseName, String messages, HttpStatus httpStatus) {
        String message = System.lineSeparator() + "     MENSAJE AL USUARIO: " + messages + System.lineSeparator() + "----------------------------------" + System.lineSeparator();
        LOGGER.log(Level.SEVERE, message);
        String[] path = request.getDescription(false).split(Pattern.quote("="), 2);
        ErrorResponse errorResponse = new ErrorResponse("" + new Timestamp(new Date().getTime()), "" +
                httpServletResponseNumber, httpServletResponseName, Arrays.asList(messages.split(",")), path[1]);
        return new ResponseEntity<>(errorResponse, httpStatus);
    }

    private void showExceptionMessage(Exception ex) {
        String message = System.lineSeparator()
                + "     MENSAJE: " + ex.getMessage()
                + System.lineSeparator();
        LOGGER.log(Level.SEVERE, message);
    }

}
