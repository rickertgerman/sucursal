# REST API Error

## Códigos de estado para la API

* 200 - OK
* 400 - Bad Request
* 404 - Not Found 
* 500 - Internal Server Error

## Formato de Response en caso de Errores

Los errores predeterminado incluyen una marca de tiempo de cuándo ocurrió el error, el código de estado HTTP, un
título (el campo de error), un mensaje (que está en blanco de forma predeterminada) y la ruta de URL donde ocurrió el
error.

Ejemplo:
```javascript
{
    "timestamp": "2020-07-30 13:05:09.649",
    "status": "404",
    "error": "NOT FOUND",
    "messages": [
        "No se encontró sucursal con Id 5."
    ],
    "path": "/api/sucursales/5"
}
```

## Manejo de Excepciones

Se crea la clase RestResponseEntityExceptionHandler, la cual es una clase que proporciona un manejo centralizado de
excepciones.

ResponseEntityExceptionHandler es una clase base conveniente para las clases @ControllerAdvice que desean proporcionar
un manejo centralizado de excepciones en todos los métodos @RequestMapping a través de los métodos @ExceptionHandler.






