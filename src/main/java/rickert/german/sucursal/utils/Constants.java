package rickert.german.sucursal.utils;

/**
 * Clase que proporciona constantes que se utilizan en el proyecto.
 *
 * @author: Rickert German
 * @version: 15/05/2021
 */
public class Constants {

    // Error Message
    public static final String ERROR_MESSAGE_NOT_FOUND_URL = "RUTA NO VÁLIDA.";
    public static final String ERROR_MESSAGE_INVALID_FIELD = "CAMPO/S NO VÁLIDOS. VERIFIQUE Y VUELVA A INTENTAR.";
    public static final String ERROR_MESSAGE_INTERNAL_SERVER_ERROR = "ERROR INTERNO.";
    public static final String ERROR_MESSAGE_MISSING_PARAMETERS = "FALTAN PARÁMETROS O NO SON COMPATIBLES.";

    public static final String HTTP_SERVLET_RESPONSE_BAD_REQUEST = "BAD REQUEST";
    public static final String HTTP_SERVLET_RESPONSE_NOT_FOUND = "NOT FOUND";
    public static final String HTTP_SERVLET_RESPONSE_INTERNAL_SERVER_ERROR = "INTERNAL SERVER ERROR";
}
