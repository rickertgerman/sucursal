package rickert.german.sucursal.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rickert.german.sucursal.exceptions.InvalidFieldException;
import rickert.german.sucursal.exceptions.NotFoundException;
import rickert.german.sucursal.model.Sucursal;
import rickert.german.sucursal.service.SucursalService;

/**
 * Clase controladora para manejar las peticiones http.
 *
 * @author: Rickert German
 * @version: 15/05/2021
 */
@RestController
@RequestMapping(value = "/api/sucursales")
public class SucursalController {

    private final SucursalService sucursalService;

    public SucursalController(SucursalService sucursalService) {
        this.sucursalService = sucursalService;
    }

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> findAll() {
        return ResponseEntity.ok(sucursalService.findAll());
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> findOne(@PathVariable long id) throws NotFoundException {
        return ResponseEntity.ok(sucursalService.findById(id));
    }

    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> create(@RequestBody Sucursal sucursal) throws InvalidFieldException {
        return new ResponseEntity<>(sucursalService.create(sucursal), HttpStatus.CREATED);
    }

    @PutMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> update(@RequestBody Sucursal sucursal) throws NotFoundException {
        return ResponseEntity.ok(sucursalService.update(sucursal));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) throws NotFoundException {
        sucursalService.deleteById(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "/{latitude}/{longitude}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> findNearestSucursal(@PathVariable Double latitude, @PathVariable Double longitude) throws NotFoundException {
        return ResponseEntity.ok(sucursalService.findNearestSucursal(latitude, longitude));
    }

}
