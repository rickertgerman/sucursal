package rickert.german.sucursal.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import rickert.german.sucursal.model.Sucursal;

public interface SucursalRepository extends JpaRepository<Sucursal, Long> {

    @Query("SELECT CASE WHEN COUNT(s) > 0 THEN TRUE ELSE FALSE END FROM Sucursal s WHERE s.id = ?1")
    boolean selectExistsById(Long id);


    @Query("SELECT CASE WHEN COUNT(s) > 0 THEN TRUE ELSE FALSE END FROM Sucursal s WHERE s.direccion LIKE ?1")
    boolean selectExistsByDireccion(String direccion);
}