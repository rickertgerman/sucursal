package rickert.german.sucursal.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.cors.CorsConfiguration;

import java.util.Arrays;
import java.util.Collections;

/**
 * Clase que proporciona una forma de habilitar la seguridad HTTP en Spring.
 * Para ello se extiende de WebSecurityConfigurerAdapter para proporcionar una configuración en el método configure(HttpSecurity http).
 *
 * @author: Rickert German
 * @version: 15/05/2021
 */
@EnableWebSecurity
@Configuration
class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors().configurationSource(request -> {
            CorsConfiguration cors = new CorsConfiguration();
            cors.setAllowedOrigins(
                    Collections.singletonList("*"));
            cors.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE", "OPTIONS"));
            cors.setAllowedHeaders(Collections.singletonList("*"));
            cors.setAllowedOrigins(Collections.singletonList("*"));
            return cors;
        })
                .and()
                .csrf().disable()
                .exceptionHandling()
                .and()
                .authorizeRequests()

                // SUCURSAL
                .antMatchers("/api/sucursales/**").permitAll()

                .antMatchers("/v2/api-docs", "/swagger-ui/**", "/swagger*/**").permitAll();
    }

}