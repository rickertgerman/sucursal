package rickert.german.sucursal.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "sucursal")
public class Sucursal extends AbstractEntity {
    @Column(name = "direccion", nullable = false)
    private String direccion;
    @Column(name = "latitud", nullable = false)
    private Double latitud;
    @Column(name = "longitud", nullable = false)
    private Double longitud;

    public Sucursal(Long id, String direccion, Double latitud, Double longitud) {
        super(id);
        this.direccion = direccion;
        this.latitud = latitud;
        this.longitud = longitud;
    }

}
