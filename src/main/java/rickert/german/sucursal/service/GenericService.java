package rickert.german.sucursal.service;

import rickert.german.sucursal.exceptions.InvalidFieldException;
import rickert.german.sucursal.exceptions.NotFoundException;

import java.util.List;

public interface GenericService<C> {
    List<C> findAll();

    C findById(Long id) throws NotFoundException;

    C create(C c) throws InvalidFieldException;

    C update(C c) throws NotFoundException;

    void deleteById(Long id) throws NotFoundException;

}