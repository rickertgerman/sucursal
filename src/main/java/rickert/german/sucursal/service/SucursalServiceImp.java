package rickert.german.sucursal.service;

import org.springframework.stereotype.Service;
import rickert.german.sucursal.exceptions.InvalidFieldException;
import rickert.german.sucursal.exceptions.NotFoundException;
import rickert.german.sucursal.model.Sucursal;
import rickert.german.sucursal.repository.SucursalRepository;

import java.util.List;
import java.util.Optional;

/**
 * Clase que maneja la lógica de negocio.
 *
 * @author: Rickert German
 * @version: 15/05/2021
 */
@Service
public class SucursalServiceImp implements SucursalService {
    private final SucursalRepository sucursalRepository;

    public SucursalServiceImp(SucursalRepository sucursalRepository) {
        this.sucursalRepository = sucursalRepository;
    }

    @Override
    public List<Sucursal> findAll() {
        return sucursalRepository.findAll();
    }

    @Override
    public Sucursal findById(Long id) throws NotFoundException {
        Optional<Sucursal> sucursal = sucursalRepository.findById(id);
        if (!sucursal.isPresent())
            throw new NotFoundException("No se encontró sucursal con Id " + id + ".");
        return sucursal.get();
    }

    @Override
    public Sucursal create(Sucursal sucursal) throws InvalidFieldException {
        boolean existsDireccion = sucursalRepository.selectExistsByDireccion(sucursal.getDireccion());
        if (existsDireccion)
            throw new InvalidFieldException("Ya existe Sucursal con la dirección proporcionada.");

        return sucursalRepository.save(sucursal);
    }

    @Override
    public Sucursal update(Sucursal sucursal) throws NotFoundException {
        boolean existsId = sucursalRepository.selectExistsById(sucursal.getId());
        if (!existsId)
            throw new NotFoundException("No se encontró sucursal con Id " + sucursal.getId() + ".");

        return sucursalRepository.save(sucursal);
    }

    @Override
    public void deleteById(Long id) throws NotFoundException {
        boolean existsId = sucursalRepository.selectExistsById(id);
        if (!existsId)
            throw new NotFoundException("No se encontró sucursal con Id " + id + ".");

        sucursalRepository.deleteById(id);
    }

    @Override
    public Sucursal findNearestSucursal(Double latitude, Double longitude) throws NotFoundException {
        List<Sucursal> sucursales = sucursalRepository.findAll();

        if (sucursales.isEmpty())
            throw new NotFoundException("No se encontraron Sucursales.");

        // Inicializar sucursal más cercana y distancia
        Sucursal nearestSucursal = null;
        double minDistance = -1.0;

        for (Sucursal sucursal : sucursales) {
            double distance = distanceBetweenTwoPoints(latitude, longitude, sucursal.getLatitud(), sucursal.getLongitud());
            if (minDistance == -1.0) {
                minDistance = distance;
                nearestSucursal = sucursal;
            } else {
                if (distance < minDistance) {
                    minDistance = distance;
                    nearestSucursal = sucursal;
                }
            }
        }

        return nearestSucursal;
    }

    private static double distanceBetweenTwoPoints(double lat1, double lon1, double lat2, double lon2) {
        lat1 = Math.toRadians(lat1);
        lon1 = Math.toRadians(lon1);
        lat2 = Math.toRadians(lat2);
        lon2 = Math.toRadians(lon2);

        double earthRadius = 6371.01; // Kilometros
        return earthRadius * Math.acos(Math.sin(lat1) * Math.sin(lat2) + Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon1 - lon2));
    }

}