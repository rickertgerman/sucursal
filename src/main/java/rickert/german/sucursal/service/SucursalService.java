package rickert.german.sucursal.service;

import rickert.german.sucursal.exceptions.NotFoundException;
import rickert.german.sucursal.model.Sucursal;

public interface SucursalService extends GenericService<Sucursal> {
    Sucursal findNearestSucursal(Double latitude, Double longitude) throws NotFoundException;
}
